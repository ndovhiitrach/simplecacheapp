import cache.api.NettyServer;
import cache.api.RacletteHandler;
import cache.api.inbound.Action;
import cache.api.inbound.RequestData;
import cache.store.RocksDBHandler;
import io.netty.buffer.ByteBuf;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Assert;
import org.junit.Test;
import utils.MultithreadingHelper;
import utils.client.NettyClient;

public class RacletteOpChannelIT {
  private final String DB_DIR = "test-rocks-db";

  @Test
  public void test() {
    EmbeddedChannel embeddedChannel = new EmbeddedChannel();

    embeddedChannel
        .pipeline()
        .addLast(
            new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)),
            new ObjectEncoder(),
            new RacletteHandler(DB_DIR, true));

    RequestData msg = new RequestData();
    msg.setAction(Action.CREATE);
    msg.setStringValue("key1");
    msg.setBinaryValue("It's raclette".getBytes());

    // Encodes RequestData to ByteBuf
    embeddedChannel.writeOutbound(msg);
    ByteBuf encodedRequestData = embeddedChannel.readOutbound();

    embeddedChannel.writeInbound(encodedRequestData);

    boolean racletteExists =
        RocksDBHandler.getInstance().getRocksDB(DB_DIR).keyMayExist("key1".getBytes(), null);
    Assert.assertTrue(racletteExists);
  }

  @Test
  public void testNettyServer() throws Exception {
    ExecutorService service = Executors.newFixedThreadPool(1);
    service.execute(
        () -> {
          try {
            new NettyServer().run(8098);
          } catch (Exception e) {
            throw new RuntimeException(e);
          }
        });
    List<Runnable> runnableList =
        Stream.generate(
                () ->
                    (Runnable)
                        (() -> {
                          try {
                            NettyClient.run(8098);
                          } catch (Exception e) {
                            throw new RuntimeException(e);
                          }
                        }))
            .limit(100)
            .collect(Collectors.toList());

    MultithreadingHelper.executeConcurrent(runnableList);
  }
}
