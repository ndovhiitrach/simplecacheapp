package benchmark;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import utils.MultithreadingHelper;
import utils.client.NettyClient;

@Measurement(iterations = 1)
@Warmup(iterations = 1)
@Fork(value = 1, warmups = 1)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.AverageTime)
public class ClientServerBenchmark {
  /**
   * Before to start this benchmark test you need to run server {@link
   * cache.RaclettePersistentCacheApplication}
   */
  public static void main(String[] args) throws Exception {
    org.openjdk.jmh.Main.main(args);
  }

  @Benchmark
  public void testNettyServer(BenchmarkState state) throws Exception {
    MultithreadingHelper.executeConcurrent(state.runnableList);
  }

  @State(Scope.Benchmark)
  public static class BenchmarkState {
    @Param({"1", "10", "50", "100"})
    private int threadsNumber;

    private List<Runnable> runnableList;

    @Setup()
    public void setup() {
      runnableList =
          Stream.generate(
                  () ->
                      (Runnable)
                          () -> {
                            try {
                              NettyClient.run(8097);
                            } catch (Exception e) {
                              throw new RuntimeException(e);
                            }
                          })
              .limit(threadsNumber)
              .collect(Collectors.toList());
    }
  }
}
