package benchmark;

import static java.util.Objects.requireNonNull;

import cache.store.RocksDBRepositoryImpl;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import utils.MultithreadingHelper;

@Measurement(iterations = 1)
@Warmup(iterations = 1)
@Fork(value = 1, warmups = 1)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.AverageTime)
public class RocksDBRepoBenchmark {

  public static void main(String[] args) throws Exception {
    org.openjdk.jmh.Main.main(args);
    //      Options opt = new OptionsBuilder()
    //              .include(RocksDBRepoBenchmark.class.getSimpleName())
    //
    //                                  .addProfiler(GCProfiler.class)
    //              .build();
    //
    //      new Runner(opt).run();
  }

  @Benchmark
  public void a_testsave(BenchmarkState state) throws InterruptedException {
    List<Runnable> runnableList =
        IntStream.range(0, state.threadsNumber)
            .mapToObj(
                index ->
                    (Runnable)
                        (() -> {
                          RocksDBRepositoryImpl rocksDBRepository =
                              new RocksDBRepositoryImpl(state.dbDir.toString(), true);
                          String key = "key" + index;
                          rocksDBRepository.save(key, state.entry);
                        }))
            .collect(Collectors.toList());
    MultithreadingHelper.executeConcurrent(runnableList);
  }

  @Benchmark
  public void b_testRead(BenchmarkState state, Blackhole blackhole) throws InterruptedException {
    List<Runnable> runnables =
        IntStream.range(0, state.threadsNumber)
            .mapToObj(
                index ->
                    (Runnable)
                        (() -> {
                          RocksDBRepositoryImpl rocksDBRepository =
                              new RocksDBRepositoryImpl(state.dbDir.toString(), true);
                          String key = "key" + index;
                          blackhole.consume(rocksDBRepository.find(key));
                        }))
            .collect(Collectors.toList());
    MultithreadingHelper.executeConcurrent(runnables);
  }

  @Benchmark
  public void c_testDelete(BenchmarkState state) throws InterruptedException {
    List<Runnable> runnables =
        IntStream.range(0, state.threadsNumber)
            .mapToObj(
                index ->
                    (Runnable)
                        (() -> {
                          RocksDBRepositoryImpl rocksDBRepository =
                              new RocksDBRepositoryImpl(state.dbDir.toString(), true);
                          String key = "key" + index;
                          rocksDBRepository.delete(key);
                        }))
            .collect(Collectors.toList());
    MultithreadingHelper.executeConcurrent(runnables);
  }

  @State(Scope.Benchmark)
  public static class BenchmarkState {
    private File dbDir;
    private Byte[] entry;
    private List<Runnable> runnables = new ArrayList<>();

    @Param({"1", "10", "50", "100"})
    private int threadsNumber;

    @Setup()
    public void setup() throws IOException {
      InputStream inputStream = getClass().getClassLoader().getResourceAsStream("binary_sample");
      byte[] sample = new byte[requireNonNull(inputStream).available()];
      IOUtils.read(inputStream, sample);
      dbDir = FileUtils.getTempDirectory();
      entry = ArrayUtils.toObject(sample);
    }
  }
}
