package utils.client;

import static java.util.Objects.requireNonNull;

import cache.api.inbound.Action;
import cache.api.inbound.RequestData;
import cache.api.outbound.ResponseData;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ClientHandler extends ChannelInboundHandlerAdapter {
  private static final Logger LOG = LogManager.getLogger(ClientHandler.class);

  @Override
  public void channelActive(ChannelHandlerContext ctx) throws IOException {
    InputStream inputStream = getClass().getClassLoader().getResourceAsStream("binary_sample");
    byte[] sample = new byte[requireNonNull(inputStream).available()];
    IOUtils.read(inputStream, sample);

    RequestData data = new RequestData();
    data.setAction(Action.CREATE);
    data.setStringValue("it's raclette key!");
    data.setBinaryValue(sample);
    ChannelFuture future = ctx.writeAndFlush(data);
  }

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object responseData) throws Exception {
    ResponseData response = (ResponseData) responseData;
    System.out.println(String.format("************%s************", (response.getStatus())));
    if (Objects.nonNull(response.getMessage())) {
      System.out.println(response.getMessage());
    }

    RequestData data = new RequestData();
    data.setAction(Action.READ);
    data.setStringValue("it's raclette key!");
    ctx.writeAndFlush(data);
  }

  @Override
  public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
    RequestData data = new RequestData();
    data.setAction(Action.DELETE);
    data.setStringValue("it's raclette key!");
    ChannelFuture future = ctx.writeAndFlush(data);
    future.addListener(ChannelFutureListener.CLOSE);
  }
}
